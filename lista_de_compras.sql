

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `lista_de_compras`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `categorias`
--

CREATE TABLE `categorias` (
  `id` int NOT NULL,
  `nome` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `categorias`
--

INSERT INTO `categorias` (`id`, `nome`) VALUES
(1, 'Alimentos'),
(2, 'Higiene Pessoal'),
(3, 'Limpeza');

-- --------------------------------------------------------

--
-- Estrutura da tabela `lista`
--

CREATE TABLE `lista` (
  `id` int NOT NULL,
  `mes` varchar(25) NOT NULL,
  `produtoid` int NOT NULL,
  `quantidade` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `lista`
--

INSERT INTO `lista` (`id`, `mes`, `produtoid`, `quantidade`) VALUES
(1, 'Janeiro', 1, 200),
(2, 'Janeiro', 2, 150),
(3, 'Janeiro', 3, 100),
(4, 'Janeiro', 4, 50),
(5, 'Janeiro', 5, 1000),
(6, 'Janeiro', 6, 500),
(7, 'Janeiro', 7, 500),
(8, 'Janeiro', 8, 500),
(9, 'Janeiro', 9, 100),
(10, 'Janeiro', 10, 50),
(11, 'Janeiro', 11, 50),
(12, 'Fevereiro', 12, 250),
(13, 'Fevereiro', 3, 50),
(14, 'Fevereiro', 13, 50),
(15, 'Fevereiro', 5, 1000),
(16, 'Fevereiro', 6, 500),
(17, 'Fevereiro', 14, 500),
(18, 'Fevereiro', 15, 301),
(19, 'Fevereiro', 16, 300),
(20, 'Março', 17, 1200),
(21, 'Março', 18, 500),
(22, 'Março', 19, 500),
(23, 'Março', 20, 500),
(24, 'Março', 21, 100),
(25, 'Março', 11, 100),
(26, 'Abril', 22, 350),
(27, 'Abril', 23, 100),
(28, 'Abril', 24, 50),
(29, 'Abril', 25, 500),
(30, 'Abril', 8, 500),
(31, 'Abril', 6, 500),
(32, 'Abril', 26, 100),
(33, 'Maio', 27, 10001),
(34, 'Maio', 28, 2000),
(35, 'Maio', 29, 1000),
(36, 'Maio', 30, 1000),
(37, 'Maio', 31, 500),
(38, 'Maio', 2, 500),
(39, 'Maio', 32, 500),
(40, 'Maio', 33, 100),
(41, 'Maio', 34, 100),
(42, 'Maio', 35, 500),
(43, 'Maio', 5, 500),
(44, 'Maio', 36, 500),
(45, 'Maio', 37, 100),
(46, 'Maio', 38, 100);

-- --------------------------------------------------------

--
-- Estrutura da tabela `produtos`
--

CREATE TABLE `produtos` (
  `id` int NOT NULL,
  `nome` varchar(255) NOT NULL,
  `categoriaid` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `produtos`
--

INSERT INTO `produtos` (`id`, `nome`, `categoriaid`) VALUES
(1, 'Arroz', 1),
(2, 'Feijão', 1),
(3, 'Pão de forma', 1),
(4, 'Nutella', 1),
(5, 'Geléria de morango', 1),
(6, 'Pão de forma', 1),
(7, 'Queijo minas', 1),
(8, 'Ovos', 1),
(9, 'Iogurte', 1),
(10, 'Pasta de Amendoim', 1),
(11, 'Filé Mignon', 1),
(12, 'Chocolate ao leite', 1),
(13, 'Doritos', 1),
(14, 'Morango', 1),
(15, 'Leite desnatado', 1),
(16, 'Brócolis', 1),
(17, 'Tomate', 1),
(18, 'Arroz integral', 1),
(19, 'Feijão', 1),
(20, 'Filé de frango', 1),
(21, 'Berinjela', 1),
(22, 'Pepino', 1),
(23, 'Papel Higiênico', 2),
(24, 'Creme dental', 2),
(25, 'Sabonete Protex', 2),
(26, 'Escova de dente', 2),
(27, 'Papel Higiênico', 2),
(28, 'Creme dental', 2),
(29, 'Sabonete Dove', 2),
(30, 'Enxaguante bocal', 2),
(31, 'Fio dental', 2),
(32, 'Escova de dente', 2),
(33, 'Creme dental', 2),
(34, 'Protex', 2),
(35, 'Papel Higiênico', 2),
(36, 'Shampoo', 2),
(37, 'Desinfetante', 3),
(38, 'Veja multiuso', 3),
(39, 'Sabão em pó', 3),
(40, 'Rodo', 3),
(41, 'Pano de chão', 3),
(42, 'Detergente', 3),
(43, 'Sabão em pó', 3),
(44, 'Esponja de aço', 3),
(45, 'Diabo verde', 3),
(46, 'MOP', 3);

--
-- Índices para tabelas despejadas
--

--
-- Índices para tabela `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `lista`
--
ALTER TABLE `lista`
  ADD PRIMARY KEY (`id`);

--
-- Índices para tabela `produtos`
--
ALTER TABLE `produtos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de tabelas despejadas
--

--
-- AUTO_INCREMENT de tabela `categorias`
--
ALTER TABLE `categorias`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de tabela `lista`
--
ALTER TABLE `lista`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT de tabela `produtos`
--
ALTER TABLE `produtos`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
