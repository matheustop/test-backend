<?php

if (!isset($argc) || is_null($argc))
{ 
    //echo "Acessível somente via linha de comando.";
    //exit();

}

$lista_de_compras = include("lista-de-compras.php");

$nova_lista = ordenarPorMeses($lista_de_compras);

$nova_lista = ordenarPorCategorias($nova_lista);

$nova_lista = ordenarPorQuantidades($nova_lista);

$nova_lista = removerMesesVazios($nova_lista);

$nova_lista = corrigirPalavras($nova_lista);

salvarBanco($nova_lista);


function salvarBanco($lista){

    foreach($lista as $mes => $categorias){
        foreach($categorias as $categoria => $produtos){
            foreach($produtos as $nome => $quantidade){
                
                $produtosBD[$categoria][] = $nome; 
                $categoriasBD[] = corrigirCategorias($categoria);

                
                if(is_array(@$produtosID)){
                    if(!in_array($nome, @$produtosID)){
                        $produtosID[] = $nome;
                    }
                } else {
                    $produtosID[] = $nome;
                }
                
                if(is_array(@$categoriasID)){
                    if(!in_array($categoria, @$categoriasID)){
                        $categoriasID[] = $categoria;
                    }
                } else {
                    $categoriasID[] = $categoria;
                }

                $listaBD[] = array("mes"=>corrigirMes($mes), "produto"=>$nome, "quantidade"=>$quantidade); 
            }
        }
    }

    $categoriasBD = array_unique($categoriasBD);

    foreach($categoriasBD as $line){
        $categoriasBD2[] = $line;
    }

    foreach($produtosBD as $categoria => $produto){
        if(@$produtosBD2[$categoria] != $produto){
            $produtosBD2[$categoria] = $produto;
        }
    }

    salvarCategorias($categoriasBD2);

    salvarProdutos($produtosBD2, $categoriasID);

    salvarLista($listaBD, $produtosID);

}


function conectarBanco(){

    $servername = "db";
    $username = "root";
    $password = "test";
    $database = "lista_de_compras";

    try {
        $conn = new PDO("mysql:host=$servername;dbname=".$database, $username, $password);
        return $conn;
      } catch(PDOException $e) {
        echo "Connection failed: " . $e->getMessage();
      }

    return $conn;
}

function salvarCategorias($categorias){
    $conn = conectarBanco();
    foreach($categorias as $categoria){
        echo $categoria."<br>";
        $sql = "INSERT INTO categorias (nome) VALUES (?)";
        $stmt= $conn->prepare($sql);
        $stmt->execute([$categoria]);
    }
}

function salvarProdutos($produtos, $categorias){
    $conn = conectarBanco();
    foreach($produtos as $categoria => $produtos){
        foreach($produtos as $produto){
            $categoriaID = array_search($categoria, $categorias);
            $categoriaID = $categoriaID + 1;
            $sql = "INSERT INTO produtos (nome, categoriaid) VALUES (?,?)";
            $stmt= $conn->prepare($sql);
            $stmt->execute([$produto, $categoriaID]);
        }
    }
}

function salvarLista($lista, $produtos){
    $conn = conectarBanco();
    foreach($lista as $line){
            $produtoID = array_search($line["produto"], $produtos);
            $produtoID = $produtoID + 1;
            //echo $produtoID . " - " . $line["mes"] . " - " . $line["quantidade"] . "<br>"; 
            $sql = "INSERT INTO lista (mes, produtoid, quantidade) VALUES (?,?,?)";
            $stmt= $conn->prepare($sql);
            $stmt->execute([$line["mes"], $produtoID, $line["quantidade"]]);
    }
}



function corrigirMes($mes){
    $meses = array(
        "janeiro" => "Janeiro",
        "fevereiro" => "Fevereiro",
        "marco" => "Março",
        "abril" => "Abril",
        "maio" => "Maio",
        "junho" => "Junho",
        "julho" => "Julho",
        "agosto" => "Agosto",
        "setembro" => "Setembro",
        "outubro" => "Outubro",
        "novembro" => "Novembro",
        "dezembro" => "Dezembro"
    );
    return $meses[$mes];
}

function corrigirCategorias($categoria){
    $categorias = array(
        "alimentos" => "Alimentos",
        "higiene_pessoal" => "Higiene Pessoal",
        "limpeza" => "Limpeza"
    );
    return $categorias[$categoria];
}

function removerMesesVazios($lista){
    foreach($lista as $mes => $categorias){
        foreach($categorias as $categoria => $produtos){
            if(is_array(@$produtos)){
                if(count($produtos)>0){
                    $novo_array[$mes][$categoria] = $produtos;
                }
            }
        }
    }
    return $novo_array;
}

function ordenarPorMeses($lista){
    foreach($lista as $mes => $dados){
        $mes_numero = gerarNumeroDoMes($mes);
        $novo_array[$mes_numero] = array("mes"=>$mes, "dados"=>$dados);
    }
    ksort($novo_array);
    foreach($novo_array as $mes_numero2 => $dados2){
        $array_resultado[$dados2["mes"]] = $dados2["dados"];
    }
    return $array_resultado;
}

function ordenarPorCategorias($lista){
    foreach($lista as $mes => $dados){
        ksort($dados);
        $novo_array[$mes] = $dados;
    }
    return $novo_array;
}

function ordenarPorQuantidades($lista){
    foreach($lista as $mes => $categorias){
        foreach($categorias as $categoria => $produtos){
            arsort($produtos);
            $novo_array[$mes][$categoria] = $produtos;
        }
    }
    return $novo_array;
}

function corrigirPalavras($lista){
    $palavras = array(
        "Papel Higiênico" => "Papel Hignico",
        "Brócolis" => "Brocolis",
        "Chocolate ao leite" => "Chocolate ao leit",
        "Sabão em pó" => "Sabao em po"        
    );
    foreach($lista as $mes => $categorias){
        foreach($categorias as $categoria => $produtos){
            foreach($produtos as $nome => $quantidade){
                $palavra_correta = array_search($nome, $palavras);
                if($palavra_correta){
                    $nome = $palavra_correta;
                }
                $novo_array[$mes][$categoria][$nome] = $quantidade;
            }
        }
    }
    return $novo_array;
}

function gerarNumeroDoMes($nome){

    $meses = array(
        "janeiro" => 1,
        "fevereiro" => 2,
        "marco" => 3,
        "abril" => 4,
        "maio" => 5,
        "junho" => 6,
        "julho" => 7,
        "agosto" => 8,
        "setembro" => 9,
        "outubro" => 10,
        "novembro" => 11,
        "dezembro" => 12
    );

    return $meses[$nome];
}